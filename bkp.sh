#/bin/bash
echo $@
OPERATION=$1
BASEDIR="${BASEDIR:-/tmp}"

upload(){
    FILENAME=$1
    gsutil cp -r $FILENAME "$BUCKET_URI/$FILENAME"
}

download(){
    FILENAME=$1
    gsutil cp -r "$BUCKET_URI/$FILENAME" $FILENAME
}

opBackup(){
    mongodump --host $MONGO_HOST -v --gzip -o $BACKUP_NAME &&
    upload $BACKUP_NAME
}

opRestore(){
    download $1 &&
    mongorestore --host $MONGO_HOST -v --drop $1
}

if [ -z "$BUCKET_URI" ]; then
  echo "Variável BUCKET_URI não está definida";
  exit 11;
fi

if [ -n "$GOOGLE_APPLICATION_CREDENTIALS" ]; then
  echo "Efetuando login no google com service account de: $GOOGLE_APPLICATION_CREDENTIALS"
  gcloud auth activate-service-account --key-file $GOOGLE_APPLICATION_CREDENTIALS
fi

case $OPERATION in
dump)
    TIMESTAMP=`date +%F-%H%M`
    BACKUP_NAME="$BASEDIR/$TIMESTAMP"
    echo "Executando BACKUP destino $BACKUP_NAME";
    opBackup $BACKUP_NAME
;;
restore)
    DUMP_NAME=$2;
    echo "Executando RESTORE de $DUMP_NAME";
    opRestore $DUMP_NAME
;;
*) echo "Operação [$OPERATION] desconhecida"; exit 10;;
esac