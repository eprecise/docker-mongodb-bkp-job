#/bin/bash
#printf "deb [arch=amd64] https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.2 multiverse" > /etc/apt/sources.list
export DEBIAN_FRONTEND=noninteractive

apt update
apt install curl apt-utils apt-transport-https tar bash ca-certificates openssl unzip xz-utils -y

# Gcloud CLI
echo "deb http://packages.cloud.google.com/apt cloud-sdk-bionic main" | tee -a /etc/apt/sources.list.d/google-cloud-sdk.list
curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -

apt update && apt install google-cloud-sdk  -y

