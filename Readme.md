# Ferramenta de Backup e Restore do MongoDB

Trabalha integrado ao GCP para storage e compactando com xz

## Modo de uso

A Variável BUCKET_URI deve estar setada, o formato é gs://bucket_name. As operações disponíveis são:

* dump
* restore